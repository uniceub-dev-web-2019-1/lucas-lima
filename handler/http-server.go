package main

import (
	"log"
	"net/http"
	"time"
)

func serverStart() {
	server := http.Server{Addr: "172.22.51.19:8085",
		Handler:      createRouter(),
		ReadTimeout:  100 * time.Microsecond,
		WriteTimeout: 200 * time.Microsecond,
		IdleTimeout:  50 * time.Microsecond}

	log.Print(server.ListenAndServe())
}
