package main

import (
	"github.com/gorilla/mux"
)

func createRouter() (r *mux.Router) {
	r = mux.NewRouter()

	//1. GET /users?name=[qualquer nome]
	r.HandleFunc("/users", UserHandler).Methods("GET").Queries("name", "{[a-z]}")

	// 2. POST /users
	r.HandleFunc("/users", UserHandler).Methods("POST").Headers("Content-Type")
	
	//3. PUT /users (header Opcional)
	r.HandleFunc("/users", UserHandler).Methods("PUT").Headers("Content-Type?")
	
	return
}
