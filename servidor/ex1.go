package main

import (
	"net/http"
	"time"
)

func main() {

	server := http.Server{Addr: "172.22.51.19:8083",
						  ReadTimeout: 100 * time.Millisecond,
						  WriteTimeout: 200 * time.Millisecond,
						  IdleTimeout: 50 * time.Millisecond}
	http.HandleFunc("/req", request)
	server.ListenAndServe()
}

func request(res http.ResponseWriter, req *http.Request) {
	m := req.Method
	host := req.Host
	path := req.URL.String()
	ct := req.Header.Get("Content-Type")

	res.Write([]byte("Method: " + m + "\nHost: " + host + "\nPath: " + path + "\nContent-Type(Header): " + ct))
}
